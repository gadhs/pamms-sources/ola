= 1400 Support Services
:policy-number: 1400
:policy-title: Support Services
:revision-date: 05/31/2024
:next-review: 05/31/2026

include::partial$policy-header.adoc[]

== Policy

The policy of the Department of Human Services is to prepare for emergencies and natural disasters through continuous planning, training, exercises, and taking corrective action in an effort to ensure effective coordination during incidents.

== Authority

There is created the position of commissioner of human services. The commissioner shall be the chief administrative officer of the department and be both appointed and removed by the board, subject to the approval of the Governor.
Subject to the general policy established by the board, the commissioner shall supervise, direct, account for, organize, plan, administer, and execute the functions vested in the department.

https://law.justia.com/codes/georgia/2010/title-49/chapter-2/article-1/49-2-1/[O.C.G.A. 49-2-1 (b)]

== References

None

== Applicability

All of the Department

== Definitions

None

== Responsibilities

The Director of Emergency Management is responsible for generating and revising procedures and manuals as needed.

== History

POL 1400 previously under the Office of Facilities and Support Services.
Policy was last reviewed 10/20/2021.

== Evaluation

The Office of Legislative Affairs and Outreach evaluates the policy annually by providing updates to the DHS Emergency Plan, procedures, and continuity plans.